import serial
import time
import json
from datetime import datetime

def ser_read():
        try:
            ser = serial.Serial('COM3', 115200, timeout=1)
            data = str(ser.readline())
        except:          
            ser.close()
            data = ''
        data = data.replace("b'","") 
        data = data.replace("\\r\\n'","")
        data = data.split('_')
        #print(data)
        return data    

def telemetria():
        data = ['']
        while len(data) < 11:
            data = ser_read()
        data_json = {}
        data_json['telemetria'] = []
        data_json['time'] = str(datetime.now())
        data_json['eventos'] = ''
        targer = ['modulo','Tension L1-N', 'Tension L2-N', 'Tension L3-N', 'L1', 'L2','L3','E_ACUMULADA','FPL1','FPL2','FPL3']
        for tg,dt in zip(targer, data):
            try:
                dt = float(dt)
                data_json[tg]= str(dt)
                if (tg == 'Tension L1-N' or tg == 'Tension L2-N' or tg == 'Tension L3-N') and dt >= 800:
                    data_json['eventos'] = data_json['eventos']+', '+'alta tensión en '+ tg
                if (tg == 'L1' or tg == 'L2' or tg == 'L3') and dt >= 460:    
                    data_json['eventos'] = data_json['eventos']+', '+'alta tension en '+ tg
            except:    
                data_json[tg]= str(dt)
        return data_json
    
"""
    def telemetria():
        data = ['']
        while len(data) < 11:
            data = ser_read()
        data_json = {}
        data_json['telemetria'] = []
        data_json['time'] = str(datetime.now())
        data_json['eventos'] = ''
        targer = ['modulo','Tension L1-N', 'Tension L2-N', 'Tension L3-N', 'L1', 'L2','L3','E_ACUMULADA','FPL1','FPL2','FPL3']
        for tg,dt in zip(targer, data):
            try:
                dt = float(dt)
                data_json['telemetria'].append({tg: str(dt)})
                if (tg == 'Tension L1-N' or tg == 'Tension L2-N' or tg == 'Tension L3-N') and dt >= 800:
                    data_json['eventos'] = data_json['eventos']+', '+'alta tensión en '+ tg
                if (tg == 'L1' or tg == 'L2' or tg == 'L3') and dt >= 460:    
                    data_json['eventos'] = data_json['eventos']+', '+'alta tension en '+ tg
            except:    
                data_json['telemetria'].append({tg: dt})
        return data_json    """