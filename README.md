## Firmware - Technical Test
#### Ing. Brayan Andru Montenegro Embus
#### 3175750088
###### Para este desarrollo de utilizo una tarjeta M5stack Core2, para simular el ingreso de datos FreeRTOS con ESP32, de 4 dispositivos, en cada uno se simula la recolección de datos de 11 variables, por medio de un sistema aleatorio, esté se desarrollo en visual studio code en cual se incluye dentro de los archivos de 
![fw_ttest_Brayan_Andru_Montenegro_Embus](https://gitlab.com/mabdeveloper/fw_ttest_Brayan_Andru_Montenegro_Embus/-/tree/main)
###### dado a un un problema con el g++, la tarjeta se programo con arduino (los archivos se incluyen tambien) para tener la ejecución. la entrega de datos hacia el PC se realiza por medio del puerto serial, dado que esta la tarjeta Core 2 no cuenta con wifi ni tarjeta de red. Esta información es recibida por medio de dos script de Python, en el puerto com3, en el script Prueba se realiza la conexión con Azure IoT el cual se realiza por la conexion string, para preparar el mensaje se llama al script leer_serial el cual establece comunicación con el puerto y estructura los datos en tipo Json, este es devuelto a el script Prueba para ser enviado a Azure. Cuando ya se encuentra en Azure se realiza la conexión con Azure Stream Analytics job, quien permite verificar lo que se ha recibido.

### Esquema.

![Esquema](https://gitlab.com/mabdeveloper/iot_solution_brayan_montenegro/-/blob/main/Esquema.png)

#### Ejecución
###### Para ejecutar el sistemas se debe programar una tarjeta ESP32 con los archivos multitask, task1 task2, task3 y task4, esta entregara por medio del puerto serial la información, posteriormente se debe ejecutar el script prueba.py, y si se desea se puede sustituir la conexión string, si no este enviara los datos a una cuenta de Azure, lo cual se vere reflejado en el letrero que indica el envío de la información. 

### Resultados.

![Video](https://www.youtube.com/watch?v=ifdpVU9De1Q)

![Json extraído desde Azure](https://gitlab.com/mabdeveloper/iot_solution_brayan_montenegro/-/blob/main/cosmos-db-prueba.json)
